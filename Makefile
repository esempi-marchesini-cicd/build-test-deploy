all: eseguibile_test app

eseguibile_test: test.o funzioni.o
	g++ -o eseguibile_test test.o funzioni.o

app: main.o funzioni.o
	g++ -o app main.o funzioni.o

main.o: main.cpp funzioni.h
	g++ -c main.cpp

test.o: test.cpp  funzioni.h
	g++ -c test.cpp

funzioni.o: funzioni.cpp funzioni.h
	g++ -c funzioni.cpp
