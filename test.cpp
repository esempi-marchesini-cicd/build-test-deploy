#define CATCH_CONFIG_MAIN

#include "catch.hpp"

#include "funzioni.h"

TEST_CASE("fattoriale") {
  REQUIRE(fattoriale(1) == 1);
  REQUIRE(fattoriale(2) == 2);
  REQUIRE(fattoriale(5) == 120);
  REQUIRE(fattoriale(10) == 8);
  REQUIRE(fattoriale(10) == 3628800);
}